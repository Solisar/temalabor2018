﻿using ActionGameFramework.Health;
using Core.Utilities;
using UnityEngine;
using UnityEngine.AI;

namespace TowerDefense.Towers.Projectiles
{
    /// <summary>
    /// Implementation of hitscan projectile
    /// The principle behind this weapon is that it instantly attacks enemies
    /// </summary>
    [RequireComponent(typeof(Damager))]
    public class HitscanSlowAttack : HitscanAttack
    {
        /// <summary>
        /// The actual attack of the hitscan attack.
        /// Early returns from the method if the there is no enemy to attack.
        /// </summary>
        protected override void DealDamage()
        {
            Poolable.TryPool(gameObject);

            if (m_Enemy == null)
            {
                return;
            }

            // effects
            ParticleSystem pfxPrefab = m_Damager.collisionParticles;
            var attackEffect = Poolable.TryGetPoolable<ParticleSystem>(pfxPrefab.gameObject);
            attackEffect.transform.position = m_Enemy.position;
            attackEffect.Play();

            
            m_Enemy.TakeDamage(m_Damager.damage, m_Enemy.position, m_Damager.alignmentProvider);
            m_Enemy.GetComponent<NavMeshAgent>().speed = (m_Enemy.GetComponent<NavMeshAgent>().speed) - ((m_Enemy.GetComponent<NavMeshAgent>().speed) / 10);
            m_PauseTimer = true;
        }
        
    }
}